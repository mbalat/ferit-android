package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button play_game, show_rules, view_scores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        play_game = findViewById(R.id.game);
        show_rules = findViewById(R.id.rules);
        view_scores = findViewById(R.id.score);

        play_game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity1();
            }
        });

        show_rules.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity2();
            }
        });

        view_scores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Activity3();
            }
        });
    }

    public void Activity1(){
        Intent intent = new Intent(this, Game.class);
        startActivity(intent);
    }

    public void Activity2(){
        Intent intent = new Intent(this, Rules.class);
        startActivity(intent);
    }


    public void Activity3(){
        Intent intent = new Intent(this, Score.class);
        startActivity(intent);
    }
}
