package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class Score extends AppCompatActivity {

    ListView scoreboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        scoreboard = findViewById(R.id.scoreboard);

        ArrayList<String> scoreboard_list = new ArrayList<>();

        scoreboard_list.add(Game.scoreboard_basic);

        for(int i = 0; i< Game.results.size(); i++){
            scoreboard_list.add(Game.results.get(i));
        }

        ArrayAdapter array_adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, scoreboard_list);
        scoreboard.setAdapter(array_adapter);
    }
}
