package ViewHolder;

import android.view.ContextMenu;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.firebasecontact.MainActivity;
import com.example.firebasecontact.R;

public class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener{

    public TextView text_contact, text_number, text_address, text_city;

    public ContactViewHolder(@NonNull View itemView) {
        super(itemView);

        text_contact = itemView.findViewById(R.id.text_contact);
        text_number = itemView.findViewById(R.id.text_number);
        text_address = itemView.findViewById(R.id.text_address);
        text_city = itemView.findViewById(R.id.text_city);

        itemView.setOnCreateContextMenuListener(this);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {

        menu.setHeaderTitle("Option Menu");
        menu.add(0,0,getAdapterPosition(), "Edit");
        menu.add(0,1,getAdapterPosition(), "Delete");
        menu.add(0,2,getAdapterPosition(), "Show Location");

    }
}
