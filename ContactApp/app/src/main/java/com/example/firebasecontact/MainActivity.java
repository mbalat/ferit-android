package com.example.firebasecontact;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import Model.Contact;
import ViewHolder.ContactViewHolder;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private TextView addressTv, cityTv;

    FirebaseDatabase database;
    DatabaseReference contactDb;

    FirebaseRecyclerOptions<Contact> options;
    FirebaseRecyclerAdapter<Contact, ContactViewHolder> adapter;

    // fab button action
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);
        fab = findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, InputActivity.class);
                startActivity(intent);
            }
        });

        database = FirebaseDatabase.getInstance();
        contactDb = database.getReference("Contact");

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        showContact();
    }

    private void showContact() {

        options = new FirebaseRecyclerOptions.Builder<Contact>()
                .setQuery(contactDb, Contact.class)
                .build();

        adapter = new FirebaseRecyclerAdapter<Contact, ContactViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ContactViewHolder holder, int position, @NonNull Contact model) {

                holder.text_contact.setText(model.getContact());
                holder.text_number.setText(model.getNumber());
                holder.text_address.setText(model.getAddress());
                holder.text_city.setText(model.getCity());
            }

            @NonNull
            @Override
            public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

                View itemView = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.contact_row, viewGroup, false);
                return new ContactViewHolder(itemView);
            }
        };

        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        if(item.getTitle().equals("Edit")) {
            showEditDialog(adapter.getRef(item.getOrder()).getKey(), adapter.getItem(item.getOrder()));
        } else if(item.getTitle().equals("Delete")){
            deleteContact(adapter.getRef(item.getOrder()).getKey());
        } else if(item.getTitle().equals("Show Location")){
            addressTv = findViewById(R.id.text_address);
            cityTv = findViewById(R.id.text_city);
            String addressMessage = addressTv.getText().toString();
            String cityMessage =  cityTv.getText().toString();
            Intent intent = new Intent(this, LocationActivity.class);
            intent.putExtra("ADDRESS_MESSAGE", addressMessage);
            intent.putExtra("CITY_MESSAGE", cityMessage);
            startActivity(intent);
        }
        return super.onContextItemSelected(item);
    }

    private void deleteContact(String key){

        contactDb.child(key).removeValue();
    }

    private void showEditDialog(final String key, Contact item) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Edit");
        builder.setMessage("Please update the fields");

        View update_layout = LayoutInflater.from(this).inflate(R.layout.custom_layout, null);

        final EditText edt_update_contact = update_layout.findViewById(R.id.edit_update_contact);
        final EditText edt_update_number = update_layout.findViewById(R.id.edit_update_number);
        final EditText edt_update_address = update_layout.findViewById(R.id.edit_update_address);
        final EditText edt_update_city = update_layout.findViewById(R.id.edit_update_city);

        edt_update_contact.setText(item.getContact());
        edt_update_number.setText(item.getNumber());
        edt_update_address.setText(item.getAddress());
        edt_update_city.setText(item.getCity());


        builder.setView(update_layout);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String contact = edt_update_contact.getText().toString();
                String number = edt_update_number.getText().toString();
                String address = edt_update_address.getText().toString();
                String city = edt_update_city.getText().toString();

                Contact mcontact = new Contact(contact, number, address, city);
                contactDb.child(key).setValue(mcontact);

                Toast.makeText(MainActivity.this, "Contact is updated!", Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
            }
        });

        builder.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.delete_all) {
            contactDb.removeValue();
        }
        return super.onOptionsItemSelected(item);
    }
}
