package com.example.firebasecontact;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import Model.Contact;

public class InputActivity extends AppCompatActivity {

    private EditText edit_contact, edit_number, edit_address, edit_city;
    private Button btn_add;

    FirebaseDatabase database;
    DatabaseReference contactDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        edit_contact = findViewById(R.id.edit_contact);
        edit_number = findViewById(R.id.edit_number);
        edit_address = findViewById(R.id.edit_address);
        edit_city = findViewById(R.id.edit_city);

        btn_add = findViewById(R.id.btn_add);

        database = FirebaseDatabase.getInstance();
        contactDb = database.getReference("Contact");


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveToFirebase();
            }
        });

    }
    private void saveToFirebase() {

        String contact = edit_contact.getText().toString();
        String number = edit_number.getText().toString();
        String address = edit_address.getText().toString();
        String city = edit_city.getText().toString();


        if(!TextUtils.isEmpty(contact) && !TextUtils.isEmpty(number) && !TextUtils.isEmpty(address) && !TextUtils.isEmpty(city)) {

            Contact mcontact = new Contact(contact, number, address, city);

            contactDb.push().setValue(mcontact).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {

                    Toast.makeText(InputActivity.this, "Contact is added.", Toast.LENGTH_SHORT).show();

                    edit_contact.setText("");
                    edit_number.setText("");
                    edit_address.setText("");
                    edit_city.setText("");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {

                    Toast.makeText(InputActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {

            Toast.makeText(this, "All fields should be filled.", Toast.LENGTH_SHORT).show();
        }
    }
}
