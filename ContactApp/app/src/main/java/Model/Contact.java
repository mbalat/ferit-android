package Model;

public class Contact {

    private String contact;
    private String number;
    private String address;
    private String city;

    public Contact() {
    }

    public Contact(String contact, String number, String address, String city) {
        this.contact = contact;
        this.number = number;
        this.address = address;
        this.city = city;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
