package Model;

public class City {
    private String city;
    private Double lat;
    private Double lng;

    public City(String city, Double lat, Double lng) {
        this.city = city;
        this.lat = lat;
        this.lng = lng;
    }

    public City() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }
}
